﻿using System;
using System.Collections.Generic;

using HangmanFFLib;

using static System.Console;

namespace HangmanFF
{
    class Program
    {
        static readonly List<string> DefaultWords = new List<string>()
        {
            "Powershop",
            "Flux Federation"
        };

        static void Main(string[] args)
        {
            WriteLine("Welcome to HangmanFF!");
            //The FF is for Flux Federation

            WriteLine();
            WriteLine("Commands are: \r\n" +
                "'new'  Start a new game.\r\n" +
                "'exit' Exits the application, can be used mid game if you get frustrated.");

            while(true)
            {
                string input = Prompt("Your command? ");
                if (string.IsNullOrWhiteSpace(input))
                    continue;
                switch (input)
                {
                    case "new":
                        {
                            PlayGame();
                            break;
                        }
                    case "exit":
                        {
                            PoliteExit();
                            return;
                        }
                    case "?":
                        {
                            WriteLine("Commands are: \r\n" +
                                "'new'  Start a new game.\r\n" +
                                "'exit' Exits the application, can be used mid game if you get frustrated.");
                            break;
                        }
                    default:
                        {
                            WriteLine($"Sorry, but '{input}' isn't a command I know. Try again.");
                            break;
                        }
                }
            }
        }

        static string Prompt(string message)
        {
            Write(message);
            return ReadLine().Split()[0].ToLower();
        }

        static void PlayGame()
        {
            Game game = new Game(DefaultWords);
            WriteLine("Let the game begin! You may type 'clues' at any time to see which letters you've already tried.");
            while (!game.GameOver && !game.Victory)
            {
                string masked = MaskSolution(game.Solution, '*');
                WriteLine($"Your word is: {masked} and you have {game.TriesRemaining} tr{((game.TriesRemaining > 1) ? "ies" : "y")} remaining.");
                string input = Prompt("Guess a charachter: ");
                switch (input)
                {
                    case "exit":
                        {
                            PoliteExit();
                            return;
                        }
                    case "clues":
                        {
                            WriteLine((game.AllreadyTried.Count > 0) ? $"You have already tried : {String.Join(", ", game.AllreadyTried)}" : "You havent started yet!");
                            break;
                        }
                    default:
                        {
                            ProcessGuess(game, input);
                            break;
                        }
                }
            }
            WriteLine((game.Victory) ? Game.VictoryMessage : Game.LossMessage);
        }

        static void ProcessGuess(Game game, string input)
        {
            GuessResult result = null;
            try
            {
                result = game.Guess(input);
            }
            catch (InvalidInputException iiex)
            {
                WriteLine($"{iiex.Message} You entered: '{iiex.InvalidCharachter}'");
                return;
            }

            WriteLine($"You entered: '{result.Charachter.ToString()}' and revealed {result.CharachtersRevealed} charachter{((result.CharachtersRevealed == 1) ? string.Empty : "s")}.");
        }

        static string MaskSolution(char?[] solution, char mask)
        {
            string maskedText = string.Empty;
            foreach (char? c in solution)
                maskedText += c ?? mask;

            return maskedText;
        }

        static void PoliteExit()
        {
            WriteLine("Your out of here! Bye! (Press a key)");
            ReadKey();
            Environment.Exit(0);
        }
    }
}
