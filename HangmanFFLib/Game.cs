﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HangmanFFLib
{
    public class Game
    {
        #region Error String Constants
        public const string GameAllreadyOver = "Sorry, but the game is already over!"; //Opted not to remind the player they lost. UX. 
        public const string GameAllreadyWon = "You've allready won, stop guessing!";
        public const string ToManyOrNotLetter = "Guess may only contain a single alpha charachter!";
        public const string CharachterAllreadyTried = "You have already tried that charachter!";

        public const string NoWordsInList = "The provided word list contains no words to choose from.";
        #endregion

        public static string VictoryMessage = "Congradulations, you won!";
        public static string LossMessage = "Sorry, you lost.";

        public bool GameOver { get { return (TriesRemaining == 0); } }

        public bool Victory { get { return (solution.RemainingMasked == 0); } }

        public int IncorrectTriesAllowed { get; private set; }
        public int TriesRemaining
        {
            get
            {
                return (IncorrectTriesAllowed - (guesses.Where(g => !g.Successfull).Count()));
            }
        }        
        public List<char> AllreadyTried { get { return guesses.Select(g => g.Charachter).ToList(); } }

        public char?[] Solution { get { return solution.Value; } }

        private readonly List<GuessResult> guesses = new List<GuessResult>();
        private readonly MaskedAnswer solution = null;
        
        /// <summary>
        /// Constructs a new game, selecting a word from the supplied wordList.
        /// </summary>
        /// <param name="wordList">The list of words to select possible answers from.</param>
        /// <param name="triesAllowed">The number of failed guesses the user is allowed.</param>
        public Game(List<string> wordList, int triesAllowed = 10)
        {
            if (wordList == null)
                throw new NullReferenceException("wordList");

            if (wordList.Count == 0)
                throw new Exception(NoWordsInList);

            IncorrectTriesAllowed = triesAllowed;
            solution = selectWord(wordList);
            findMatches(' '); //Reveal any spaces.
        }

        /// <summary>
        /// Provide a guess for evaluation.
        /// </summary>
        /// <param name="charachter">The charachter you wish evaluated.</param>
        /// <returns>A GuessResult object describing the evaluation results.</returns>
        public GuessResult Guess(char charachter)
        {
            if (GameOver)
                throw new InvalidInputException(charachter.ToString(), GameAllreadyOver);
            
            if (Victory)
                throw new InvalidInputException(charachter.ToString(), GameAllreadyWon);

            if (allreadyTried(charachter))
                throw new InvalidInputException(charachter.ToString(), CharachterAllreadyTried);
            
            return findMatches(charachter);
        }
        /// <summary>
        /// Provide a guess for evaluation.
        /// </summary>
        /// <param name="input">String to attempt to convert into a single character and evaluate.</param>
        /// <returns>A GuessResult object describing the evaluation results.</returns>
        public GuessResult Guess(string input)
        {
            if (!char.TryParse(input, out char charachter))
                throw new InvalidInputException(input, ToManyOrNotLetter);

            return Guess(charachter);
        }
        
        private GuessResult findMatches(char charachter)
        {
            //Initially this was all in the Reveal() method of MaskedAnswer, but its interaction with GuessResult seemed wrong, MaskedAnswer became responsible for GuessResults population.
            GuessResult guess = new GuessResult(charachter);

            int index = solution.IndexOf(guess.CharachterLower);
            while (index != -1)
            {
                solution.Reveal(index);
                guess.CharachtersRevealed++;
                index = solution.IndexOf(guess.CharachterLower, index + 1);
            }

            if (!char.IsWhiteSpace(charachter)) guesses.Add(guess);
            return guess;
        }

        private MaskedAnswer selectWord(List<string> wordList)
        {
            return new MaskedAnswer(wordList[new Random().Next(0, wordList.Count)]);                
        }

        private bool allreadyTried(char charachter)
        {
            return (guesses.Where(g => g.CharachterLower == charachter).Count() > 0);
        }
    }

    internal class MaskedAnswer
    {
        //Considered exposing the below two strings as internal but am trying to Obey Single responsibility principle.
        private string originalWord = null;
        private string loweredWord = null;
        private char?[] mask;

        public char?[] Value { get { return mask; } }
        public int RemainingMasked
        {
            get
            {
                //Oddly, the alternate syntax: mask.Select(m => m == null).Count() - doesnt seem to work?
                //return (from m in mask
                //        where m == null
                //        select m).Count();
                return mask.Where(m => m == null).Count();
            }
        }

        public MaskedAnswer(string word)
        {
            originalWord = word;
            loweredWord = originalWord.ToLower();
            mask = new char?[word.Length];
        }

        /// <summary>
        /// View a particular charachter, not used in my implimentation. 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public char? this[uint index]
        {
            get
            {
                if (index >= originalWord?.Length)
                    throw new IndexOutOfRangeException();

                return mask[index];
            }
        }

        public int IndexOf(char charachter, int start = 0)
        {
            return loweredWord.IndexOf(charachter, start);
        }

        public void Reveal(int index)
        {
            mask[index] = originalWord[index];
        }
    }

    public class GuessResult
    {
        public readonly char Charachter;
        internal readonly char CharachterLower;

        public int CharachtersRevealed { get; internal set; }

        public bool Successfull { get { return CharachtersRevealed > 0; } }

        public GuessResult(char guess)
        {
            if (!char.IsLetter(guess) && !char.IsWhiteSpace(guess))
                throw new InvalidInputException(guess.ToString(), Game.ToManyOrNotLetter);

            Charachter = guess;
            CharachterLower = Charachter.ToString().ToLower()[0];
        }
    }


    public class InvalidInputException : Exception
    {
        public readonly string InvalidCharachter;

        public InvalidInputException(string charachter, string message) : base(message)
        {
            InvalidCharachter = charachter;
        }
    }
}
